(ns matrix-auth.login-test
  (:require [clojure.test :refer :all]
            [matrix-auth.login :refer :all]
            [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [clojure.java.io :as jio]
            [buddy.core.keys :as keys]
            [buddy.sign.jws :as jws]
            [buddy.sign.jwt :as jwt]
            [clj-http.util :as hutil]
            [matrix-auth.login :as login]
            [cheshire.core :as json]
            [clj-http.client :as client])
  )

(comment

  (ulog/spewer
   (login {:privkey-path "privkey.pem"
           :pubkey-path "pubkey.pem"}
          (-> "/home/cust/spaz/keys/nowplaying.edn"
              slurp
              edn/read-string)))


  (ulog/spewer
   ;; note it'll keywordize keys but leave vals as strings
   (jwt/unsign   "eyJhbGciOiJFUzI1NiJ9.eyJ1c2VybmFtZSI6ImFzcyIsImhvc3RuYW1lIjoibWF0cml4LnNwYXoub3JnIn0.MEQCIEItd7YcxdjMwE_byG8JO0oKtmuIHNkIWj5eHDMfADoDAiA7C9y4gLVVKTg68a7_Vx-TMKiC3asyE_PxPrzV7rQ7Cg"
                 (keys/public-key "pubkey.pem")
                 {:alg :es256}))
  )
