# matrix-auth

A matrix application service gateway to a non-standard chat service

## Installation

TODO

## Usage

TODO

    $ java -jar matrix-auth-0.1.0-standalone.jar [args]

## Options

FIXME: listing of options this app accepts.

## Examples

...

### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2017-2018 ken restivo <ken@restivo.org>

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
