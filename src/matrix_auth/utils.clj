(ns matrix-auth.utils
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [yada.yada :as y]
            [bidi.bidi :as b]
            [schema.core :as s]
            [hiccup.core :as html]
            [taoensso.timbre :as log]
            [cheshire.core :as json]))


;; TODO: move to utilza
(def errors
  {500 {:description "Error"
        :produces #{"text/html" "text/plain;q=0.9"}
        :response (let [msg "Something went sideways"]
                    (fn [ctx]
                      (-> ctx :error log/error)
                      (case (y/content-type ctx)
                        "text/html" (html/html [:h2 msg])
                        (str msg \newline))))}
   404 {:description "Not found"
        :produces #{"text/html" "text/plain;q=0.9"}
        :response (let [msg "Missing in action"]
                    (fn [ctx]
                      (case (y/content-type ctx)
                        "text/html" (html/html [:h2 msg])
                        (str msg \newline))))}})
