(ns matrix-auth.server
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [mount.core :as mount]
            [taoensso.timbre :as log]
            [yada.yada :as y]
            [matrix-auth.logging :as alog] ;; for dependency
            [matrix-auth.api :as api]
            ))


(defn start!
  [{:keys [jwt web]}]
  ;; TODO: redact
  (log/info "starting up web server" web)
  (y/listener (api/app jwt) web))


(defn stop!
  [{:keys [close]}]
  (log/info "Shutting down web server")
  (close))

(mount/defstate ^{:on-reload :noop}
  server 
  :start (->> (mount/args) start!)
  :stop (stop! server))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  ;; LAAAZY, just use mount yo
  (def foo (:close server))

  (log/info "WTF")

  (foo)

  (->> (mount/args) :web)

  (ulog/spewer
   (->> (mount/args) :jwt api/app))


  
  )
