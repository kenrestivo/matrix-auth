(ns matrix-auth.api
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [yada.yada :as y]
            [bidi.bidi :as b]
            [matrix-auth.login :as login]
            [matrix-auth.utils :as u]
            [schema.core :as s]
            [hiccup.core :as html]
            [taoensso.timbre :as log]
            [cheshire.core :as json]
            [clj-http.client :as client]))





(defn login-response
  "Given jwt settings and a context, attempts to log in to Matrix using those settings/creds.
   Returns a map with the token if successful, or the context with a 403 response if not."
  [jwt-settings ctx]
  ;; don't ever log this, it'd be a security nightmare
  (if-let [res (some->> ctx
                        :parameters
                        :body
                        (login/login jwt-settings))]
    {:token res} 
    (-> ctx
        :response
        (assoc :status 403))))


(defn login-password
  "Given jwt settings,
  Returns a resource map for the login endpoint."
  [jwt-settings]
  (y/resource {:show-stack-traces? false
               :produces [{:media-type #{"application/json"}
                           :charset "UTF-8"}]
               :responses u/errors
               :methods {:post {:parameters {:body {:username s/Str
                                                    :password s/Str
                                                    (s/optional-key :hostname)  s/Str}}
                                :consumes [{:media-type #{"application/json"}
                                            :charset "UTF-8"}]
                                :response (partial login-response jwt-settings)}}}))


(defn api-routes
  "Given jwt settings,
  returns resource map for ping and login endpoints."
  [jwt-settings]
  [["/ping" (y/as-resource {:response "pong"} )]
   ["/login"  (login-password jwt-settings)]
   ])


(defn app
  "Given jwt settings,
  returns application top-level resource map."
  [jwt-settings]
  ["" [["/" (y/as-resource  "Nothing here\n")]
       ["/foobar" (y/as-resource "whut")]
       ["/api" (api-routes jwt-settings)]
       ]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(comment

  (log/info "foo")

  (ulog/spewer
   (api-routes {}))

  
  (ulog/spewer
   (app ))


  (->> "/api/ping"
       (b/match-route (app))
       ulog/spewer)



  )
