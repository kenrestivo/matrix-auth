(ns matrix-auth.core
  (:gen-class)
  (:import [java.net URLEncoder])  
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [cheshire.core :as json]
            [taoensso.timbre :as log]
            [matrix-auth.logging :as alog]
            [matrix-auth.server :as srv]
            [mount.core :as mount]
            [clj-http.client :as client]))



(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(comment

  
  (log/info :wtf)
  (log/debug :wtf)
  
  (ulog/catcher
   (mount/stop)
   (mount/start-with-args {:web {:port 3000}
                           :jwt {:privkey-path "privkey.pem"
                                 :pubkey-path "pubkey.pem"}
                           :log {:spit-filename "/tmp/foo.log4j"
                                 :tracing? true
                                 :level :debug
                                 :appenders [:spit]}}))

  )
