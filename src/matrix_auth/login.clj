(ns matrix-auth.login  (:require [utilza.repl :as urepl]
                                 [matrix-clj.matrix.net :as net]
                                 [utilza.log :as ulog]
                                 [yada.yada :as y]
                                 [bidi.bidi :as b]
                                 [clojure.edn :as edn]
                                 [schema.core :as s]
                                 [buddy.core.keys :as keys]
                                 [buddy.sign.jws :as jws]
                                 [buddy.sign.jwt :as jwt]
                                 [taoensso.timbre :as log]
                                 [cheshire.core :as json]
                                 [clj-http.client :as client]))

(defn sign
  "Given a jwt-settings map with privkey and pubkey paths, and something to sign,
  Signs it and returns it."
  [{:keys [privkey-path pubkey-path]} payload]
  (let [priv (keys/private-key privkey-path)
        pub (keys/public-key pubkey-path)]
    ;; NOTE: MUST specify algorithm when using ec keys, or it dies
    (jwt/sign payload priv {:alg :es256})))


(defn check-creds
  "Given a creds map with username, password, and hostname,
   tries to log into matrix. Returns a token if successful, or nil if not"
  [{:keys [hostname] :as creds}]
  ;; TODO: further checks, maybe try getting user info after receiving token
  (try
    (-> creds
        (assoc :base-url (str "https://" hostname))
        net/login)
    (catch clojure.lang.ExceptionInfo e
      (-> e ex-data :body
          (log/debug (select-keys creds [:username :hostname :device-id])) ))
    (catch Throwable e
      (log/debug e))))

(defn login
  "Tries to log in to matrix. 
   Returns signed assertion of the username, hostname, device-id provided if successful,
   and nil if not."
  [jwt-settings creds]
  (when (check-creds creds)
    (sign jwt-settings (select-keys creds [:username :hostname :device-id]))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  


  (ulog/spewer
   )


  )
