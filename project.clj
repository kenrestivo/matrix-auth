(defproject matrix-auth "0.1.0-SNAPSHOT"
  :description "Authenticate services and clients against Matrix"
  :url "https://github.com/kenrestivo/matrix-auth"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [clj-http "3.9.1" :exclusions [riddley]]
                 [org.clojure/core.async "0.4.474"]
                 [aleph "0.4.6"]
                 [org.slf4j/log4j-over-slf4j "1.7.25"]
                 [org.slf4j/slf4j-simple "1.7.25"]
                 [matrix-clj "0.1.2"]
                 [yada "1.2.14"]
                 [utilza "0.1.99"]
                 [com.taoensso/timbre "4.10.0"]
                 [mount "0.1.13"]
                 [clj-time "0.14.4"]
                 [org.clojure/tools.trace "0.7.9"]
                 [cheshire "5.8.1"]]
  :main ^:skip-aot matrix-auth.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:plugins [[lein-difftest "2.0.0"]]}
             })
